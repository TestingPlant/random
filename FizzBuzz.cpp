/*
------------------------------------------------------------------------------
This software is available under 2 licenses -- choose whichever you prefer.
------------------------------------------------------------------------------
ALTERNATIVE A - MIT License
Copyright (c) 2021 TestingPlant
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
------------------------------------------------------------------------------
ALTERNATIVE B - Public Domain (www.unlicense.org)
This is free and unencumbered software released into the public domain.
Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
software, either in source code form or as a compiled binary, for any purpose,
commercial or non-commercial, and by any means.
In jurisdictions that recognize copyright laws, the author or authors of this
software dedicate any and all copyright interest in the software to the public
domain. We make this dedication for the benefit of the public at large and to
the detriment of our heirs and successors. We intend this dedication to be an
overt act of relinquishment in perpetuity of all present and future rights to
this software under copyright law.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
------------------------------------------------------------------------------
*/

#include <type_traits>
#define A(n) template <int number, int max> std::enable_if_t<number == n, void> print();

template <int number, int max> std::enable_if_t<number % 3 != 0 && number % 5 == 0 && number <= max, void> print();
template <int number, int max> std::enable_if_t<number % 3 == 0 && number % 5 != 0 && number <= max, void> print();
template <int number, int max> std::enable_if_t<number % 3 == 0 && number % 5 == 0 && number <= max, void> print();

A(98) A(97) A(94) A(92) A(91) A(89) A(88) A(86) A(83) A(82) A(79) A(77) A(76) A(74) A(73) A(71) A(68) A(67) A(64) A(62) A(61) A(59) A(58) A(56) A(53) A(52) A(49) A(47) A(46) A(44) A(43) A(41) A(38) A(37) A(34) A(32) A(31) A(29) A(28) A(26) A(23) A(22) A(19) A(17) A(16) A(14) A(13) A(11) A(8) A(7) A(4) A(2) A(1)
#undef A
#define A(n) template <int number, int max> std::enable_if_t<number == n, void> print() { \
	print<number+1, max>(); \
	static_assert(number != number, "\r\33[2KOUTPUT: " #n); \
}

template <int number, int max> std::enable_if_t<number % 3 != 0 && number % 5 == 0 && number <= max, void> print() {
	print<number+1, max>();
	static_assert(number != number, "\r\33[2KOUTPUT: Buzz");
}
template <int number, int max> std::enable_if_t<number % 3 == 0 && number % 5 != 0 && number <= max, void> print() {
	print<number+1, max>();
	static_assert(number != number, "\r\33[2KOUTPUT: Fizz");
}
template <int number, int max> std::enable_if_t<number % 3 == 0 && number % 5 == 0 && number <= max, void> print() {
	print<number+1, max>();
	static_assert(number != number, "\r\33[2KOUTPUT: FizzBuzz");
}

A(98) A(97) A(94) A(92) A(91) A(89) A(88) A(86) A(83) A(82) A(79) A(77) A(76) A(74) A(73) A(71) A(68) A(67) A(64) A(62) A(61) A(59) A(58) A(56) A(53) A(52) A(49) A(47) A(46) A(44) A(43) A(41) A(38) A(37) A(34) A(32) A(31) A(29) A(28) A(26) A(23) A(22) A(19) A(17) A(16) A(14) A(13) A(11) A(8) A(7) A(4) A(2) A(1)

int main() {
	print<1, 100>();
}
